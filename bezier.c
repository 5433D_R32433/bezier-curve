#include <SDL2/SDL.h>
#include <stdio.h>

typedef float  f32;
typedef double f64;

typedef unsigned long long u64;
typedef unsigned int       u32;
typedef unsigned short     u16;
typedef unsigned char       u8;

typedef signed long long   i64;
typedef signed int         i32;
typedef signed short       i16;
typedef signed char         i8;

// typedef size_t    usize;
// typedef ptrdiff_t isize;

typedef i32 b32;
typedef i16 b16;
typedef i8  b8;

#define true  1
#define false 0

#define EPSILON 1e-5f
#define PI      3.1415927f

#define RADIANS( degrees ) ( ( PI / 180 ) * ( degrees ) )
#define DEGREES( radians ) ( ( 180 / PI ) * ( radians ) )

#define LINE_SIZE 256
#define PATH_SIZE 256

#define UNUSED_VARIABLE( x ) ( ( void ) ( x ) )
#define ARRAY_COUNT( a )     ( sizeof ( a ) / sizeof ( ( a ) [ 0 ] ) )

#define WINDOW_WIDTH  800
#define WINDOW_HEIGHT 600

#define FPS 60
#define DELAY_SEC ( 1.0f / 60 )
#define DELAY_MS  ( ( u32 ) floorf ( DELAY_SEC * 1000.0f ) )


#define GRUVBOX_BRIGHT_RED        0xFB4934FF
#define GRUVBOX_BRIGHT_GREEN      0xB8BB26FF
#define GRUVBOX_BRIGHT_YELLOW     0xFABD2FFF
#define GRUVBOX_BRIGHT_BLUE       0x83A598FF
#define GRUVBOX_BRIGHT_PURPLE     0xD3869BFF
#define GRUVBOX_BRIGHT_AQUA       0x8EC07CFF
#define GRUVBOX_BRIGHT_ORANGE     0xFE8019FF

#define GRUVBOX_NEURAL_RED        0xFF1D24CC
#define GRUVBOX_NEURAL_GREEN      0xFF1A9798
#define GRUVBOX_NEURAL_YELLOW     0xFF2199D7
#define GRUVBOX_NEURAL_BLUE       0xFF888545
#define GRUVBOX_NEURAL_PURPLE     0xFF8662B1
#define GRUVBOX_NEURAL_AQUA       0xFF6A9D68
#define GRUVBOX_NEURAL_ORANGE     0xFF0E5DD6

#define GRUVBOX_FADED_RED         0xFF06009D
#define GRUVBOX_FADED_GREEN       0xFF0E7479
#define GRUVBOX_FADED_YELLOW      0xFF1476B5
#define GRUVBOX_FADED_BLUE        0xFF786607
#define GRUVBOX_FADED_PURPLE      0xFF713F8F
#define GRUVBOX_FADED_AQUA        0xFF587B42
#define GRUVBOX_FADED_ORANGE      0xFF033AAF

#define GRUVBOX_BRIGHT_GRAY_ZERO  0xFF8499A8
#define GRUVBOX_BRIGHT_GRAY_ONE   0xFF93AEBD
#define GRUVBOX_BRIGHT_GRAY_TWO   0xFFA1C4D5
#define GRUVBOX_BRIGHT_GRAY_THREE 0xFFB2DBEB
#define GRUVBOX_BRIGHT_GRAY_FOUR  0xFFC7F1FB

#define GRUVBOX_DARK_GRAY_ZERO    0xFF748392
#define GRUVBOX_DARK_GRAY_ONE     0xFF454950
#define GRUVBOX_DARK_GRAY_TWO     0xFF545C66
#define GRUVBOX_DARK_GRAY_THREE   0xFF646F7C
#define GRUVBOX_DARK_GRAY_FOUR    0xFFBCE5F2

#define HEX_COLOR(x) ( ( x ) >> ( 3 * 8 ) ) & 0xFF, \
                     ( ( x ) >> ( 2 * 8 ) ) & 0xFF, \
                     ( ( x ) >> ( 1 * 8 ) ) & 0xFF, \
                     ( ( x ) >> ( 0 * 8 ) ) & 0xFF

f32 f32_lerp ( f32 a, f32 b, f32 t )
{
     return a + ( b - a ) * t;
}

typedef struct vec2
{
     f32 x;
     f32 y;

} vec2;

vec2 vec2_new ( f32 x, f32 y )
{
     vec2 v = { 0 };
     v.x    = x;
     v.y    = y;
     return v;
}

vec2 vec2_add ( vec2 a, vec2 b )
{
     return vec2_new ( a.x + b.x, a.y + b.y );
}

vec2 vec2_sub ( vec2 a, vec2 b )
{
     return vec2_new ( a.x - b.x, a.y - b.y );
}

vec2 vec2_mul ( vec2 v, f32 factor )
{
     return vec2_new ( v.x * factor, v.y * factor );
}

vec2 vec2_div ( vec2 v, f32 divisor )
{
     return vec2_mul ( v, 1 / divisor );
}

f32 vec2_len ( vec2 v )
{
     return ( f32 ) sqrt ( v.x * v.x + v.y * v.y );
}

vec2 vec2_lerp ( vec2 a, vec2 b, f32 t )
{
     f32 x = f32_lerp ( a.x, b.x, t );
     f32 y = f32_lerp ( a.y, b.y, t );
     return vec2_new  ( x, y        );
}


i32 csc ( i32 code )
{
     if ( code < 0 )
     {
          fprintf ( stderr, "SDL ERROR: %s\n", SDL_GetError ( ) );
          return code;
     }

     return code;
}


void *csp ( void *p )
{
     if ( !p )
     {
          fprintf ( stderr, "SDL ERROR: %s\n", SDL_GetError ( ) );
          return 0;
     }

     return p;
}


void draw_line ( SDL_Renderer *renderer, vec2 start, vec2 end, u32 color )
{
     csc ( SDL_SetRenderDrawColor ( renderer,  HEX_COLOR ( color ) ) );
     csc ( SDL_RenderDrawLine ( renderer,
                                ( i32 ) floorf ( start.x ),
                                ( i32 ) floorf ( start.y ),
                                ( i32 ) floorf ( end.x   ),
                                ( i32 ) floorf ( end.y   ) ) );

}

void draw_fill_rectangle ( SDL_Renderer *renderer,
                           vec2 pos,
                           vec2 size,
                           u32  color )
{
     SDL_Rect rect =
     {
          ( i32 ) floorf ( pos.x ),
          ( i32 ) floorf ( pos.y ),
          ( i32 ) floorf ( size.x ),
          ( i32 ) floorf ( size.y )
     };

     csc ( SDL_SetRenderDrawColor ( renderer,  HEX_COLOR ( color ) ) );
     csc ( SDL_RenderFillRect     ( renderer, &rect ) );
}

void draw_marker ( SDL_Renderer *renderer,
                   vec2 pos,
                   u32  color )
{
     vec2 size = vec2_new ( 20, 20 );
     draw_fill_rectangle ( renderer,
                           vec2_sub ( pos, vec2_mul ( size, 0.5f ) ),
                           size,
                           color );
}



void clear_screen ( SDL_Renderer *renderer, u32 color )
{
     csc ( SDL_SetRenderDrawColor ( renderer,  HEX_COLOR ( color ) ) );
     csc ( SDL_RenderClear ( renderer ) );
}



vec2 points [ 256 ] = { 0 };
i32  pc = 0;

i32 main ( i32 argc, i8 **argv )
{
     csc ( SDL_Init ( SDL_INIT_VIDEO ) );

     SDL_Window *window = csp ( SDL_CreateWindow( "Bezier Curve!",
                                                  100,
                                                  100,
                                                  WINDOW_WIDTH,
                                                  WINDOW_HEIGHT,
                                                  SDL_WINDOW_RESIZABLE ) );

     SDL_Renderer *renderer = csp ( SDL_CreateRenderer ( window,
                                                         -1,
                                                         SDL_RENDERER_ACCELERATED ) );

     csc ( SDL_RenderSetLogicalSize ( renderer,
                                      WINDOW_WIDTH,
                                      WINDOW_HEIGHT ) );

     b32 quit = false;
     f32 t = 0;
     while ( !quit )
     {
          SDL_Event e;
          while ( SDL_PollEvent ( &e ) )
          {
               switch ( e.type )
               {

               case SDL_QUIT:
               {
                    quit = true;
               } break;

               case SDL_MOUSEBUTTONDOWN:
               {
                    switch ( e.button.button )
                    {
                    case SDL_BUTTON_LEFT:
                    {
                         points [ pc++ ] = vec2_new ( e.button.x, e.button.y );
                    } break;

                    }
               } break;

               }
          }

          clear_screen ( renderer, 0x1E1E1EFF );

          for ( i32 i = 0; i < pc; i++ )
          {
               draw_marker ( renderer, points [ i ], GRUVBOX_BRIGHT_RED );
          }

          for ( i32 i = 0; i < pc - 1; i++ )
          {
               draw_marker ( renderer,
                             vec2_lerp ( points [ i ], points [ i + 1 ], ( sinf ( t ) + 1.0f ) * 0.5f ),
                             GRUVBOX_BRIGHT_YELLOW );
          }

          SDL_RenderPresent ( renderer );

          SDL_Delay ( DELAY_MS );
          t += DELAY_SEC;
     }

     SDL_DestroyRenderer ( renderer );
     SDL_DestroyWindow   ( window );
     SDL_Quit            ( );

     return 0;
}
