@echo off

if not defined DevEnvDir (
call vc x64
)

set CFLAGS= -MTd -Z7 -GS- -Oi -Gm- -GR- -sdl- -FC -IC:\vcpkg\installed\x64-windows-static\include 

set LFLAGS= -link -SUBSYSTEM:CONSOLE -NXCOMPAT:NO -DYNAMICBASE:NO -opt:ref -opt:icf -out:bezier.exe -LIBPATH:C:\vcpkg\installed\x64-windows-static\debug\lib -LIBPATH:C:\vcpkg\installed\x64-windows-static\debug\lib\manual-link
set LIBS= SDL2-staticd.lib SDL2maind.lib
set WIN32_LIBS= opengl32.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib ws2_32.lib crypt32.lib setupapi.lib imm32.lib winmm.lib version.lib d2d1.lib ole32.lib Netapi32.lib userenv.lib dwmapi.lib wtsapi32.lib

set SOURCES= ../bezier.c

if exist debug (
rmdir /S /Q debug
mkdir debug
pushd debug
cl %CFLAGS% %SOURCES% %LFLAGS% %WIN32_LIBS% %LIBS%
popd debug
) else (
mkdir debug
pushd debug
cl %CFLAGS% %SOURCES% %LFLAGS% %WIN32_LIBS% %LIBS%
popd debug
)




