@echo off

if not defined DevEnvDir (
call vc x64
)

set CFLAGS= -MT -GS- -O2 -Oi -Gm- -GR- -sdl- -FC -IC:\vcpkg\installed\x64-windows-static\include -IC:\vcpkg\installed\x64-windows-static\include\qt5 -IC:\vcpkg\installed\x64-windows-static\include\qt5\QtWidgets -IC:\vcpkg\installed\x64-windows-static\include\qt5\QtCore -IC:\vcpkg\installed\x64-windows-static\include\qt5\QtGui

set PATH=%PATH%;C:\vcpkg\installed\x64-windows-static\tools\qt5\bin

REM  Qt5AxServer.lib 
set LFLAGS= -link -SUBSYSTEM:console -EMITPOGOPHASEINFO -DEBUG:NONE -NXCOMPAT:NO -DYNAMICBASE:NO -opt:ref -opt:icf -out:game.exe -LIBPATH:C:\vcpkg\installed\x64-windows-static\lib -LIBPATH:C:\vcpkg\installed\x64-windows-static\plugins\platforms
set LIBS= Qt5Core.lib Qt5AxBase.lib Qt5Widgets.lib Qt5AccessibilitySupport.lib Qt5MultimediaQuick.lib Qt5QuickShapes.lib Qt5AxBase.lib Qt5MultimediaWidgets.lib Qt5QuickTemplates2.lib Qt5AxContainer.lib Qt5Multimedia.lib Qt5QuickTest.lib Qt5NetworkAuth.lib Qt5QuickWidgets.lib Qt5Concurrent.lib Qt5Network.lib Qt5Quick.lib Qt5OpenGLExtensions.lib Qt5Sql.lib Qt5DBus.lib Qt5OpenGL.lib Qt5Svg.lib Qt5DesignerComponents.lib Qt5PacketProtocol.lib Qt5Designer.lib Qt5PlatformCompositorSupport.lib Qt5ThemeSupport.lib Qt5DeviceDiscoverySupport.lib Qt5PrintSupport.lib Qt5UiTools.lib Qt5EdidSupport.lib Qt5QmlDebug.lib Qt5WebSockets.lib Qt5EventDispatcherSupport.lib Qt5QmlModels.lib Qt5Widgets.lib Qt5FbSupport.lib Qt5QmlWorkerScript.lib Qt5WindowsUIAutomationSupport.lib Qt5FontDatabaseSupport.lib Qt5Qml.lib Qt5Xml.lib Qt5Gui.lib Qt5QuickControls2.lib Qt5Help.lib Qt5QuickParticles.lib qdirect2d.lib qminimal.lib qoffscreen.lib qwindows.lib zstd.lib pcre2-16.lib pcre2-32.lib pcre2-8.lib pcre2-posix.lib icudt.lib icuuc.lib icuin.lib icuio.lib icutu.lib zlib.lib iconv.lib libpng16.lib bz2.lib harfbuzz.lib double-conversion.lib freetype.lib brotlicommon-static.lib brotlidec-static.lib brotlienc-static.lib 
set WIN32_LIBS= kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib ws2_32.lib crypt32.lib setupapi.lib imm32.lib winmm.lib version.lib d2d1.lib ole32.lib Netapi32.lib userenv.lib dwmapi.lib wtsapi32.lib

set SOURCES= ../game.cpp 

if exist release (
rmdir /S /Q release
mkdir release
pushd release
cl %CFLAGS% %SOURCES% %LFLAGS% %WIN32_LIBS% %LIBS%
popd release
) else (
mkdir release
pushd release
cl %CFLAGS% %SOURCES% %LFLAGS% %WIN32_LIBS% %LIBS%
popd release
)




